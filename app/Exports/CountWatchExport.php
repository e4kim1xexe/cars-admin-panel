<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CountWatchExport implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     *
     * DB::raw('COUNT(COALESCE(car_video.watch, 0)) as Просмотры'))
     */
    public function collection()
    {

        return DB::table('car_video')
            ->select('videos.id as Id', 'videos.name as Видео', DB::raw('GROUP_CONCAT(cars.name, ", ") as Машины'), DB::raw('COUNT(COALESCE(car_video.time, 0)) as Просмотры'))
            ->leftJoin('videos', 'videos.id', '=', 'car_video.video_id')
            ->leftJoin('cars', 'cars.id', '=', 'car_video.car_id')
            ->groupBy('car_video.video_id')
            ->get();

    }

    public function headings(): array
    {
        return [
          'ID', 'Видео','Машины','Количество просмотров'
        ];
    }
}
