<?php

namespace App\Exports;

use App\Models\Video;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class WatchRecordsExport implements FromCollection, WithHeadings
{
    /**
     * @return Collection
     */
    public function collection(): Collection
    {
        return Video::leftJoin('car_video', 'videos.id', '=', 'car_video.video_id')
            ->leftJoin('cars', 'car_video.car_id', '=', 'cars.id')
            ->select('car_video.id as Id', 'videos.name as Видео')
            ->selectSub(function ($query) {
                $query->select(DB::raw('GROUP_CONCAT(cars.name, ", ")'))
                    ->from('car_video')
                    ->leftJoin('cars', 'car_video.car_id', '=', 'cars.id')
                    ->whereColumn('car_video.video_id', 'videos.id');
            }, 'Машины')
            ->selectRaw('car_video.time as time')
            ->selectRaw('car_video.repeat as Повторы')
            ->selectRaw('car_video.watch as Просмотры')
            ->whereNotNull('car_video.time')
            ->groupBy('car_video.id')
            ->orderBy('car_video.time')
            ->get();
    }


    public function headings(): array
    {
        return [
            'id',
            'Видео',
            'Машины',
            'Время',
            'Повтор',
            'Показано'
        ];
    }
}
