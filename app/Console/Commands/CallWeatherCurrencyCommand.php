<?php

namespace App\Console\Commands;

use App\ServicesImportData\Weather\Weather;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use stdClass;

class CallWeatherCurrencyCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:weather-currency';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'For getting weather date with api';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $weatherData = Cache::get('weather_data');
        if (!$weatherData) {
            $apiData = new Weather();
            $weather = new stdClass();
            $weather->main = $apiData->main;
            $weather->description = $apiData->description;
            $weather->temperature = $apiData->temperature;
            $rates = new stdClass();
            $rates->USD = $this->getCurrency()['USD'];
            $rates->KZT = $this->getCurrency()['KZT'];
            $rates->RUB = $this->getCurrency()['RUB'];
            $rates->EUR = $this->getCurrency()['EUR'];
            $weatherCurrency = new stdClass();
            $weatherCurrency->screen_brightness = $this->getBrightness();
            $weatherCurrency->weather = $weather;
            $weatherCurrency->rates = $rates;
            $weatherCurrency->city = 'Бишкек';
            Cache::put('weather_data', $weatherCurrency);
        }
    }


    /**
     * @return int
     */
    private function getBrightness(): int
    {
        $timeOne = 12;
        $nowTime = intval(now()->format('H'));
        if ($nowTime <= $timeOne) {
            return 60;
        }
        return 100;
    }

    /**
     * @return mixed
     */
    private function getCurrency(): mixed
    {
        $xmlString = Http::get('https://nbkr.kg/XML/daily.xml');
        $xml = simplexml_load_string($xmlString);
        $rates = [];
        foreach ($xml->Currency as $currency) {
            $isoCode = (string) $currency['ISOCode'];
            $value = (float) str_replace(',', '.', (string) $currency->Value);
            $rates[$isoCode] = $value;
        }
        $result = new stdClass();
        $result = $rates;
        $jsonString = json_encode($result, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
        return json_decode($jsonString, true);
    }

}
