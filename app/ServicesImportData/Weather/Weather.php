<?php

namespace App\ServicesImportData\Weather;

use Cmfcmf\OpenWeatherMap\CurrentWeather;
use Gmopx\LaravelOWM\LaravelOWM;

class Weather
{
    public $temperature;
    public $main;
    public $description;

    public function __construct()
    {
        $data = $this->getLocationData();
        $this->temperature = $data->temperature->now->getValue();
        $this->main = $data->weather->description;
        $this->description = $data->wind->speed->getDescription();
    }

    /**
     * @return CurrentWeather
     */
    private function getLocationData(): CurrentWeather
    {
        $loc = new LaravelOWM();
        return $loc->getCurrentWeather('bishkek');
    }

}
