<?php

declare(strict_types=1);

namespace App\Orchid;

use Orchid\Platform\Dashboard;
use Orchid\Platform\ItemPermission;
use Orchid\Platform\OrchidServiceProvider;
use Orchid\Screen\Actions\Menu;

class PlatformProvider extends OrchidServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @param Dashboard $dashboard
     *
     * @return void
     */
    public function boot(Dashboard $dashboard): void
    {
        parent::boot($dashboard);

        // ...
    }

    /**
     * Register the application menu.
     *
     * @return Menu[]
     */
    public function menu(): array
    {
        return [
            Menu::make('Видео')
                ->icon('bs.box-arrow-up-right')
                ->title('Навигация')
                ->route('platform.video.list'),

            Menu::make('Машины')
                ->icon('bs.box-arrow-up-right')
                ->route('platform.cars.list'),
            Menu::make('Яркость')
                ->icon('bs.box-arrow-up-right')
                ->route('platform.brightness.list'),

            Menu::make('Группировка по видео')
                ->title('Статистика')
                ->icon('bs.box-arrow-up-right')
                ->route('platform.video-sort.list'),

            Menu::make('Статистика по видео')
                ->icon('bs.box-arrow-up-right')
                ->route('platform.video-static.list'),

            Menu::make(__('Водители'))
                ->icon('bs.people')
                ->route('platform.driver.list')
                ->permission('platform.systems.users')
                ->title(__('Водители')),

            Menu::make(__('Администратор'))
                ->icon('bs.people')
                ->route('platform.systems.users')
                ->permission('platform.systems.users')
                ->title(__('Сотрудники')),
        ];
    }

    /**
     * Register permissions for the application.
     *
     * @return ItemPermission[]
     */
    public function permissions(): array
    {
        return [
            ItemPermission::group(__('System'))
                ->addPermission('platform.systems.roles', __('Roles'))
                ->addPermission('platform.systems.users', __('Users')),
        ];
    }
}
