<?php

namespace App\Orchid\Layouts\Modal;

use App\Models\Car;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class VideoWatchingsCountTable extends Table
{
    protected function iconNotFound(): string
    {
        return 'icon-table';
    }

    protected function subNotFound(): string
    {
        return 'Данные будут доступны только после того, как видео будет просмотрено хотя бы один раз';
    }

    /**
     * @return string
     */
    protected function textNotFound(): string
    {
        return __('Нету данных для отображения');
    }

    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'db';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): iterable
    {
        return [
            TD::make('#')->render(function ($target, object $loop) {
                return $loop->iteration;
            })->alignCenter(),
            TD::make('машины')->render(function ($target) {
                $car = Car::find($target->car_id);
                return $car->name;
            })->alignCenter(),
            TD::make('просмотры')->render(function ($target) {
                return $target->total_records;
            })->alignCenter(),
        ];
    }
}
