<?php

namespace App\Orchid\Layouts\Statistics;

use App\Models\Video;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class VideoStatisticsTable extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'records';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): iterable
    {
        return [
            TD::make('id')
                ->render(function ($target) {
                    return $target->id;
                }),
            TD::make('Видео')
                ->render(function ($target) {
                    $video = Video::findOrFail($target->video_id);
                    return $video->name;
                }),
            TD::make('Машины')
                ->render(function ($target) {
                    $cars = Video::findOrFail($target->video_id)
                        ->cars()
                        ->select('cars.*')
                        ->groupBy('cars.id')
                        ->get();
                    return view('video.table', compact('cars'));
                }),
            TD::make('Время')
                ->render(function ($target) {
                    return $target->time;
                }),
            TD::make('Повтор')
                ->render(function ($target) {
                    return $target->repeat;
                }),
            TD::make('Показано')
                ->render(function ($target) {
                    return $target->watch;
                })
        ];
    }
}
