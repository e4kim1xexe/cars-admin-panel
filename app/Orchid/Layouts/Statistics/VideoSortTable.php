<?php

namespace App\Orchid\Layouts\Statistics;

use App\Models\Video;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class VideoSortTable extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'videos';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): iterable
    {
        return [
            TD::make('ID')
                ->render(function (Video $video) {
                    return $video?->id;
                }),
            TD::make('Видео')
                ->render(function (Video $video) {
                    return
                        Link::make($video->name)
                            ->route('platform.video.view', ['video' => $video]);
                }),
            TD::make('Машины')
                ->render(function (Video $video) {
                    $cars = Video::findOrFail($video->id)
                        ->cars()
                        ->select('cars.*')
                        ->groupBy('cars.id')
                        ->get();
                    return view('video.table', compact('cars'));
                }),
            TD::make('Количество показов')->render(function (Video $video) {
                return Video::getAllWatchingCount($video->id);
            })

        ];
    }
}
