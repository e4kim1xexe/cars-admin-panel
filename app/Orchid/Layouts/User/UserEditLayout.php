<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\User;

use Orchid\Screen\Field;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Rows;

class UserEditLayout extends Rows
{
    /**
     * The screen's layout elements.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        return [
            Input::make('user.name')
                ->type('text')
                ->max(255)
                ->required()
                ->title(__('Имя'))
                ->placeholder(__('Name')),

            Input::make('user.email')
                ->required()
                ->title(__('Логин'))
                ->placeholder(__('Email')),

            Input::make('user.number')
                ->type('number')
                ->required()
                ->title(__('Номер телефона'))
                ->placeholder(__('Введите номер телефона')),
        ];
    }
}
