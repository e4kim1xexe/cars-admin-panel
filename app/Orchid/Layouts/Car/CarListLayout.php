<?php

namespace App\Orchid\Layouts\Car;

use App\Models\Car;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class CarListLayout extends Table
{
    protected function iconNotFound(): string
    {
        return 'icon-table';
    }

    /**
     * @return string
     */
    protected function textNotFound(): string
    {
        return __('There are no records in this view');
    }

    /**
     * @return string
     */
    protected function subNotFound(): string
    {
        return '';
    }
    protected function striped(): bool
    {
        return true;
    }
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'cars';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): iterable
    {
        return [
            TD::make('id', 'ID')
                ->render(function (Car $car) {
                    return $car?->id;
                }),
            TD::make('name', 'Имя')
                ->render(function (Car $car) {
                    return $car?->name;
                }),
            TD::make('id', 'Водитель')
                ->render(function (Car $car) {
                    if (is_null($car->driver_id) ) {
                        return 'не установлено';
                    }
                    return $car->driver->name;
                }),
            TD::make('id', 'is_online')
                ->render(function (Car $car) {
                    if ($car->is_online) {
                        return 'Онлайн';
                    }
                    return 'Не в сети';
                }),
            TD::make('id', 'Действия')
                ->render(function (Car $car) {
                    return Link::make('Подробнее')
                        ->style('background-color: pink')
                        ->route('platform.cars.list', $car);
                }),

        ];
    }
}
