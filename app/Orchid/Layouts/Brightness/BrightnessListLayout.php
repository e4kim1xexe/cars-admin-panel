<?php

namespace App\Orchid\Layouts\Brightness;

use App\Models\Brightness;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class BrightnessListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'brightnesses';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): iterable
    {
        return [
            TD::make('id', 'ID')
                ->render(function (Brightness $brightness) {
                    return $brightness?->id;
                }),
            TD::make('time', 'время')
                ->render(function (Brightness $brightness) {
                    return $brightness?->time;
                }),
            TD::make('brightness', 'яркость')
                ->render(function (Brightness $brightness) {
                    return $brightness?->brightness;
                }),
            TD::make('std', 'Действия')
                ->render(function (Brightness $brightness) {
                    return
                        Link::make('Подробнее')
                            ->style('background-color: pink')
                            ->route('platform.brightness.view', ['brightness' => $brightness]);
                }),
        ];
    }
}
