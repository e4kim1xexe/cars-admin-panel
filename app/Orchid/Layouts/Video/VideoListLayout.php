<?php

namespace App\Orchid\Layouts\Video;

use App\Models\Video;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class VideoListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'video';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): iterable
    {
        return [
            TD::make('id', 'ID')
                ->render(function (Video $video) {
                    return $video?->id;
                }),
            TD::make('name', 'название')
                ->render(function (Video $video) {
                    return $video?->name;
                }),
            TD::make('queue', 'очередь')
                ->render(function (Video $video) {
                    return $video?->queue;
                }),
            TD::make('is_playing', 'Проигрывается')
                ->render(function (Video $video) {
                    if ($video?->is_playing) {
                        return 'Да';
                    }
                    return 'Нет';
                }),
            TD::make('weather', 'Погода')
                ->render(function (Video $video) {
                    if ($video?->weather) {
                        return 'Да';
                    }
                    return 'Нет';
                }),
            TD::make('std', 'Действия')
                ->render(function (Video $video) {
                    return Link::make('Подробнее')
                        ->route('platform.video.list', $video);
                })
        ];
    }
}
