<?php

namespace App\Orchid\Layouts\Driver;

use App\Models\Driver;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class DriversLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'drivers';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): iterable
    {
        return [
            TD::make('id')->render(function (Driver $driver) {
                return $driver->id;
            }),
            TD::make('имя')->render(function (Driver $driver) {
                if ($driver->name) {
                    return $driver->name;
                }
                return 'Не установлено';
            }),
            TD::make('Логин')->render(function (Driver $driver) {
                if ($driver->login) {
                    return $driver->login;
                }
                return 'Не установлено';
            }),
            TD::make('Действия')->render(function (Driver $driver) {
                return Link::make('Подробнее')
                    ->route('platform.driver.view', ['driver' => $driver]);
            })
        ];
    }
}
