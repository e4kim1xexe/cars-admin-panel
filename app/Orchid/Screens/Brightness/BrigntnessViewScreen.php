<?php

namespace App\Orchid\Screens\Brightness;

use App\Models\Brightness;
use App\Models\Car;
use Illuminate\Http\RedirectResponse;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Screen\Sight;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class BrigntnessViewScreen extends Screen
{
    private $brightness;

    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(Brightness $brightness): iterable
    {
        $this->brightness = $brightness;
        return [
            'brightness' => $this->brightness
        ];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Яркость';
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make('Редактировать')
                ->icon('pencil')
                ->route('platform.brightness.edit', ['brightness' => $this->brightness]),
            Button::make('Удалить')
                ->icon('bs.person-fill-dash')
                ->method('remove', ['brightness' => $this->brightness->id])
                ->confirm('Вы уверены что хотите удалить яркость?')
        ];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::legend('brightness', [
                Sight::make(__('Яркость'))
                    ->render(function () {
                        return $this->brightness->brightness;
                    }),
                Sight::make(__('Время'))
                    ->render(function () {
                        return $this->brightness->time;
                    }),
                Sight::make(__('Дата добавления яркости'))
                    ->render(function () {
                        return $this->brightness->created_at;
                    }),
            ]),

        ];
    }

    public function remove(Brightness $brightness): RedirectResponse
    {
        $brightness_delete = Brightness::findOrFail($brightness->id);
        $brightness_delete->delete();
        Alert::info('Вы успешно удалили яркость');
        return redirect()->route('platform.brightness.list');
    }
}
