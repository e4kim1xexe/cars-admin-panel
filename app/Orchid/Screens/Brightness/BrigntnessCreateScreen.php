<?php

namespace App\Orchid\Screens\Brightness;

use App\Http\Requests\BrightnessRequest;
use App\Models\Brightness;
use Illuminate\Http\RedirectResponse;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class BrigntnessCreateScreen extends Screen
{
    private $brightness;

    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Добавить яркость';
    }

    /**
     * The screen's action buttons.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make(__('Добавить'))
                ->method('store')
        ];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::rows([
                Input::make('brightness')
                    ->title('яркость')
                    ->type('number'),
                Input::make('time')
                    ->title('Время')
                    ->type('time')
            ])
        ];
    }

    /**
     * @param BrightnessRequest $request
     * @return RedirectResponse
     */
    public function store(BrightnessRequest $request): RedirectResponse
    {
        $this->brightness = new Brightness();
        $this->brightness->fill($request->validated())->save();
        Alert::info(__('Успешно добавили!'));
        return redirect()->route('platform.brightness.list');
    }
}
