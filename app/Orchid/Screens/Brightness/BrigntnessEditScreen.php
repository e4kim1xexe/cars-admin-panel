<?php

namespace App\Orchid\Screens\Brightness;

use App\Http\Requests\BrightnessRequest;
use App\Models\Brightness;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class BrigntnessEditScreen extends Screen
{
    private $brightness;

    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(Brightness $brightness): iterable
    {
        $this->brightness = $brightness;
        return [

        ];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Редактировать';
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make('Сохранить')
                ->method('update', ['brightness' => $this->brightness->id])
        ];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::rows([
                Input::make('time')
                    ->title('Время')
                    ->type('time')
                ->value($this->brightness->time),
                Input::make('brightness')
                    ->title('Яркость')
                    ->type('number')
                ->value($this->brightness->brightness)
            ]),
        ];
    }

    public function update(BrightnessRequest $request, Brightness $brightness)
    {
        $brightness->fill($request->validated())->save();
        Alert::info('Успешно обновлено!');
        return redirect()->route('platform.brightness.list');
    }
}
