<?php

namespace App\Orchid\Screens\Driver;

use App\Http\Requests\DriverRequest;
use App\Http\Requests\DriverUpdateRequest;
use App\Models\Driver;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class DriversEditScreen extends Screen
{
    public $driver;

    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(Driver $driver): iterable
    {
        $this->driver = $driver;
        return [
            'driver' => $this->driver
        ];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Редактировать водителя';
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make('Сохранить')
                ->method('update', ['driver' => $this->driver])
        ];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::rows([
                Input::make('name')
                    ->value($this->driver->name)
                    ->title('Имя водителя'),
                Input::make('login')
                    ->value($this->driver->login)
                    ->title('Логин'),
                Input::make('number')
                    ->value($this->driver->number)
                    ->title('Номер телефона')
            ])
        ];
    }

    public function update(DriverUpdateRequest $request)
    {
        $this->driver->fill($request->validated())->save();
        Alert::info('Успешно обновлено!');
        return redirect()->route('platform.driver.list');
    }
}
