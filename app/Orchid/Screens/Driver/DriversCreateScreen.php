<?php

namespace App\Orchid\Screens\Driver;

use App\Http\Requests\DriverRequest;
use App\Models\Driver;
use Illuminate\Http\RedirectResponse;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class DriversCreateScreen extends Screen
{
    private $driver;

    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Добавить водителя';
    }

    /**
     * The screen's action buttons.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make(__('Добавить'))
                ->method('store')
        ];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::rows([
                Input::make('name')
                    ->title('Имя водителя')
                    ->type('string'),
                Input::make('login')
                    ->title('Логин')
                    ->required(),
                Input::make('number')
                    ->type('number')
                    ->title('Номер телефона')
                    ->required()
            ])
        ];
    }

    /**
     * @param DriverRequest $request
     * @return RedirectResponse
     */
    public function store(DriverRequest $request): RedirectResponse
    {
        $this->driver = new Driver();
        $this->driver->fill($request->validated())->save();
        Alert::info(__('Успешно добавили!'));
        return redirect()->route('platform.driver.list');
    }
}
