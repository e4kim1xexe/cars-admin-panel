<?php

namespace App\Orchid\Screens\Driver;

use App\Models\Driver;
use Illuminate\Http\RedirectResponse;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Screen\Sight;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class DriversViewScreen extends Screen
{
    private $driver;

    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(Driver $driver): iterable
    {
        $this->driver = $driver;
        return [
            'driver' => $driver
        ];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Данные водителя';
    }

    /**
     * The screen's action buttons.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make('Удалить')
                ->method('remove', ['driver' => $this->driver->id]),
            Link::make('Редактировать')
                ->route('platform.driver.edit', ['driver' => $this->driver->id])
        ];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::legend('driver', [
                Sight::make('Имя')->render(function (Driver $driver) {
                    if ($driver->name) {
                        return $driver->name;
                    }
                    return 'Не установлено';
                }),
                Sight::make('Логин')->render(function (Driver $driver) {
                    if ($driver->login) {
                        return $driver->login;
                    }
                    return 'Не установлено';
                }),
                Sight::make('Номер телефона')->render(function (Driver $driver) {
                    if ($driver->number) {
                        return $driver->number;
                    }
                    return 'Не установлено';
                })
            ])
        ];
    }

    /**
     * @param Driver $driver
     * @return RedirectResponse
     */
    public function remove(Driver $driver): RedirectResponse
    {
        $driver_to_delete = Driver::find($driver->id);
        $driver_to_delete->delete();
        Alert::info('Вы успешно удалили водителя');
        return redirect()->route('platform.driver.list');
    }
}
