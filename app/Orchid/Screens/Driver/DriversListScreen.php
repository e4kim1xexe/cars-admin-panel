<?php

namespace App\Orchid\Screens\Driver;

use App\Models\Driver;
use App\Orchid\Layouts\Driver\DriversLayout;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;

class DriversListScreen extends Screen
{
    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [
            'drivers' => Driver::paginate(10)
        ];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Водители';
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make('Добавить водителя')->route('platform.driver.create')
        ];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            DriversLayout::class
        ];
    }
}
