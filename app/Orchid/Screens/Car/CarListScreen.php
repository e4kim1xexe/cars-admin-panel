<?php

namespace app\Orchid\Screens\Car;

use App\Models\Car;
use App\Orchid\Layouts\Car\CarListLayout;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;

class CarListScreen extends Screen
{
    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [
            'cars' => Car::all()
        ];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Машины';
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make('Добавить')
                ->icon('bs.person-add')
                ->route('platform.cars.create')
        ];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            CarListLayout::class
        ];
    }
}
