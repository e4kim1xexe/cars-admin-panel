<?php

namespace app\Orchid\Screens\Car;

use App\Http\Requests\CarEditRequest;
use App\Models\Car;
use App\Models\Driver;
use App\Models\User;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class CarEditScreen extends Screen
{
    public $car;

    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(Car $car): iterable
    {
        $this->car = $car;
        return [
            'car' => $this->car
        ];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Редактирование :' . $this->car->name;
    }

    /**
     * The screen's action buttons.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make('Сохранить')
                ->method('update', ['car' => $this->car])
        ];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::rows([
                Input::make('name')
                    ->title('Название')
                    ->value($this->car->name),
                Relation::make('driver_id')
                    ->title('Водитель')
                    ->value($this->car->driver)
                    ->fromModel(Driver::class, 'name')
                    ->allowEmpty()
            ]),
        ];
    }

    public function update(CarEditRequest $request)
    {
        $this->car->fill($request->validated())->save();
        Alert::info('Успешно обновлено!');
        return redirect()->route('platform.cars.list');
    }
}
