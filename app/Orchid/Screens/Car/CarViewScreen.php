<?php

namespace app\Orchid\Screens\Car;

use App\Models\Car;
use Illuminate\Http\RedirectResponse;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Screen\Sight;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class CarViewScreen extends Screen
{
    private $car;

    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(Car $car): iterable
    {
        $this->car = $car;
        return [
            'car' => $this->car
        ];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Машина: ' . $this->car->name;
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make('Редактировать')
                ->icon('pencil')
                ->route('platform.cars.edit', ['car' => $this->car]),
            Button::make('Удалить')
                ->icon('bs.person-fill-dash')
                ->method('remove', ['car' => $this->car->id])
                ->confirm('Вы уверены что хотите удалить машину: ' . $this->car->name)
        ];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::legend('Car', [
                Sight::make(__('Название машины'))
                    ->render(function () {
                        return $this->car->name;
                    }),
                Sight::make(__('Водитель'))
                    ->render(function () {
                        if (is_null($this->car->driver->name) ){
                            return 'Не установлено';
                        }
                        return $this->car->driver->name;
                    }),
                Sight::make(__('Логин'))
                    ->render(function () {
                        if (is_null($this->car->driver->login) ){
                            return 'Не установлено';
                        }
                        return $this->car->driver->name;
                    }),
                Sight::make(__('Номер телефона'))
                    ->render(function () {
                        if (is_null($this->car->user) ){
                            return 'Не установлено';
                        }
                        return $this->car->driver->number;
                    }),
                Sight::make(__('Дата добавления авто'))
                    ->render(function () {
                        return $this->car->created_at;
                    }),
                Sight::make(__('Статус'))
                    ->render(function () {
                        if ($this->car->is_online) {
                            return 'В сети';
                        }
                        return 'Не в сети';
                    }),
            ]),

        ];
    }

    public function remove(Car $car): RedirectResponse
    {
        $car_to_delete = Car::findOrFail($car->id);
        $car_to_delete->delete();
        Alert::info('Вы успешно удалили машину');
        return redirect()->route('platform.cars.list');
    }
}
