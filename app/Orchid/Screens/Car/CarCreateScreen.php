<?php

namespace app\Orchid\Screens\Car;

use App\Http\Requests\CarCreateRequest;
use App\Models\Car;
use App\Models\Driver;
use App\Models\Post;
use App\Models\User;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class CarCreateScreen extends Screen
{
    private $car;
    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'CarCreateScreen';
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make(__('Добавить'))
                ->method('store')
        ];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::rows([
                Input::make('name')
                    ->title('Название'),
                Relation::make('driver_id')
                    ->title('Водитель')
                    ->fromModel(Driver::class, 'name'),
            ]),
        ];
    }
    public function store(CarCreateRequest $request)
    {
        $this->car = new Car();
        $this->car->fill($request->validated())->save();
        Alert::info(__('Успешно добавили!'));
        return redirect()->route('platform.cars.list');
    }
}
