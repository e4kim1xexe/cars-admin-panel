<?php

namespace App\Orchid\Screens\Video;

use App\Models\Video;
use App\Orchid\Layouts\Modal\VideoWatchingsCountTable;
use Illuminate\Support\Facades\DB;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Actions\ModalToggle;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Switcher;
use Orchid\Screen\Layouts\Modal;
use Orchid\Screen\Screen;
use Orchid\Screen\Sight;
use Orchid\Support\Facades\Layout;

class VideoListScreen extends Screen
{
    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [
            'videos' => Video::sorted()->get()
        ];
    }

    public function needPreventsAbandonment(): bool
    {
        return config('platform.prevents_abandonment', false);
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Список видео';
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make('Добавить')
                ->icon('bs.person-add')
                ->route('platform.video.create'),
        ];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [

            Layout::modal('editClient', [VideoWatchingsCountTable::class, Layout::rows([
                    Input::make('views')
                        ->title('Количество просмотров')
                        ->disabled()
                        ->style('color:black')->horizontal()
                ])]
            )->async('asyncGetClient')
                ->size(Modal::SIZE_LG)
                ->withoutApplyButton()
                ->type(Modal::TYPE_RIGHT)
                ->closeButton('Закрыть'),

            Layout::sortable('videos', [
                Sight::make(__('Название видео'))
                    ->render(function (Video $video) {
                        return
                            Link::make($video->name)
                                ->style('background-color: yellow; width:140px;')
                                ->route('platform.video.view', ['video' => $video]);
                    }),
                Sight::make('weather')
                    ->render(function (Video $video) {
                        return Switcher::make('weather')
                            ->class('weather')
                            ->id($video->id)
                            ->sendTrueOrFalse()
                            ->placeholder('погода/курс во время ролика')
                            ->value($video->weather);
                    }),
                Sight::make('weather_after')
                    ->render(function (Video $video) {
                        return
                            Switcher::make('weather_after')
                                ->class('weather-after')
                                ->id($video->id)
                                ->sendTrueOrFalse()
                                ->placeholder('погода/курс после ролика')
                                ->value($video->weather_after)
                                ->class('weather-after');
                    }),
                Sight::make('name')
                    ->render(function (Video $video) {
                        return ModalToggle::make('Просмотры: ' . Video::getAllWatchingCount($video->id) . ' ')
                            ->modal('editClient')
                            ->method('update')
                            ->modalTitle('Детальная информация о видео: ' . $video->name)
                            ->icon('wallet')
                            ->style('background-color: #c4dbf4; width:160px')
                            ->asyncParameters([
                                'video' => $video->id
                            ]);
                    }),
                Sight::make('std')
                    ->render(function (Video $video) {
                        return Link::make('Редактировать')
                            ->style('background-color: pink')
                            ->route('platform.video.edit', ['video' => $video]);
                    })
            ]),
        ];
    }

    public function asyncGetClient(Video $video): array
    {
        return [
            'db' => DB::table('car_video')
                ->select('car_id', DB::raw('count(*) as total_records'))
                ->where('video_id', $video->id)
                ->whereNotNull('time')
                ->groupBy('car_id')
                ->get(),
            'views' => Video::getAllWatchingCount($video->id),
        ];
    }
}
