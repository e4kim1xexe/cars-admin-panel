<?php

namespace App\Orchid\Screens\Video;

use App\Models\Repeat;
use App\Models\Video;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Screen\Sight;
use Orchid\Screen\TD;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class VideoViewScreen extends Screen
{
    public $video;

    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(Video $video): iterable
    {
        $this->video = $video;
        $videoLink = null;
        foreach ($this->video->attachment as $attach) {
            $videoLink = $attach->id;
        }
        return [
            'video' => $this->video,
            'cars' => $this->video->cars()->paginate(5),
            'videoLink' => $videoLink
        ];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Видео';
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make('Редактировать')
                ->icon('pencil')
                ->route('platform.video.edit', ['video' => $this->video]),
            Button::make('Удалить')
                ->icon('bs.person-fill-dash')
                ->method('remove', ['video' => $this->video->id])
                ->confirm('Вы уверены что хотите удалить видео?  ' . $this->video->name)
        ];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::legend('video', [
                Sight::make(__('Название видео'))
                    ->render(function () {
                        return $this->video->name;
                    }),
                Sight::make(__('Очередь'))
                    ->render(function () {
                        return $this->video->queue;
                    }),
                Sight::make(__('Погода'))
                    ->render(function () {
                        if (!$this->video->weather_currency) {
                            return 'Нет';
                        }
                        return 'Да';
                    }),
                Sight::make(__('Проигрывается'))
                    ->render(function () {
                        if (!$this->video->is_playing) {
                            return 'Нет';
                        }
                        return 'Да';
                    }),
                Sight::make('Видео')
                    ->render(function () {
                        $html = '';
                        foreach ($this->video->attachment as $file) {
                            $html .= "<a style='text-decoration: underline' href='$file->relativeUrl'>$file->original_name</a><br>";
                        }
                        return $html;
                    })
            ]),
            Layout::table('cars', [
                TD::make('#')
                    ->render(function ($car) {
                        return $car->id;
                    }),
                TD::make('Машина')
                    ->render(function ($car) {
                        return Link::make($car->name)->route('platform.cars.view', ['car' => $car]);
                    }),
                TD::make('Водитель')
                    ->render(function ($car) {
                        return Link::make($car->driver->name)->route('platform.driver.list', ['driver' => $car->driver]);
                    }),
            ])->title('Машины в котором')
        ];
    }

    public function remove(Video $video)
    {
        $video_to_delete = Video::findOrFail($video->id);
        $video_to_delete->delete();
        Alert::info('Вы успешно удалили видео');
        return redirect()->route('platform.video.list');
    }
}
