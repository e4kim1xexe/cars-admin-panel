<?php

namespace App\Orchid\Screens\Video;

use App\Http\Requests\VideoRequest;
use App\Models\Car;
use App\Models\Tag;
use App\Models\Video;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Matrix;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Switcher;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class VideoEditScreen extends Screen
{
    public $video;

    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(Video $video): iterable
    {
        $this->video = $video;
        $file = null;
        foreach ($video->attachment as $attach) {
            if ($attach->group === 'files') {
                $file = $attach->id;
            }
        }
        return [
            'video' => $video,
            'file' => $file
        ];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Видео';
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make('Сохранить')
                ->method('update', ['video' => $this->video])];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::rows([
                Group::make([
                    Upload::make('videos')
                        ->maxFiles(1)
                        ->title(__('Фото'))
                        ->value($this->video->attachment->first() ? $this->video->attachment->first()->id : null),
                    Relation::make('cars[]')
                        ->fromModel(Car::class, 'name')
                        ->multiple()
                        ->value($this->video->cars)
                        ->title('Выберите машины')
                        ->vertical(),
                    Input::make('name')
                        ->title('название')
                        ->value($this->video->name)
                        ->placeholder('введите название')
                ]),
                Group::make([
                    Switcher::make('weather')
                        ->sendTrueOrFalse()
                        ->title('погода')
                        ->value($this->video->weather)
                        ->placeholder('Изменить')
                        ->help('Включит/Выключить'),
                    Switcher::make('weather_after')
                        ->sendTrueOrFalse()
                        ->value($this->video->weather_after)
                        ->title('погода после видео')
                        ->placeholder('Изменить')
                        ->help('Включит/Выключить'),
                ]),
                Group::make([
                    Matrix::make('count_repeat')
                        ->title('Повторы')
                        ->columns(['время', 'количество повторений'])
                        ->fields([
                            'time' => Input::make()->type('time'),
                            'count' => Input::make()->type('number'),
                        ])->maxRows(10)->value($this->video->count_repeat)
                ])
            ])
        ];
    }

    public function update(VideoRequest $requestVal, Request $request)
    {
        $tags = $request['cars'];
        $this->video->fill($requestVal->validated())->save();
        $this->video->cars()->sync($tags);
        if (isset($request['videos'])) {
            $this->video->attachment()->sync(
                $request->input('videos', [])
            );
            $this->video->update(['file' => $this->video->attachment()->first()->url]);
        }
        Alert::info('Успешно обновлено!');
        return redirect()->route('platform.cars.list');
    }
}
