<?php

namespace App\Orchid\Screens\Video;

use App\Http\Requests\VideoRequest;
use App\Models\Car;
use App\Models\Video;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Matrix;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Switcher;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class VideoCreateScreen extends Screen
{
    private $video;

    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(Video $video): iterable
    {
        $this->video = $video;
        return [
            'video' => $video,
        ];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Добавить видео';
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make(__('Добавить'))
                ->method('store')
        ];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::rows([
                Group::make([
                    Upload::make('videos')
                        ->maxFiles(1)
                        ->title('Видео')
                        ->storage('public')->required(),
                    Relation::make('cars[]')
                        ->fromModel(Car::class, 'name')
                        ->multiple()
                        ->title('Выберите машины')
                        ->vertical(),
                    Input::make('name')
                        ->title('название')
                        ->placeholder('введите название')
                ]),
                Group::make([
                    Switcher::make('weather')
                        ->sendTrueOrFalse()
                        ->title('погода')
                        ->placeholder('Изменить')
                        ->help('Включит/Выключить'),
                    Switcher::make('weather_after')
                        ->sendTrueOrFalse()
                        ->title('погода после видео')
                        ->placeholder('Изменить')
                        ->help('Включит/Выключить'),
                ]),
                Group::make([
                    Matrix::make('count_repeat')
                        ->title(__('Повторы'))
                        ->columns([__('время'), __('количество повторений')])
                        ->fields([
                            __('time') => Input::make()->type('time'),
                            __('count') => Input::make()->type('number'),
                        ])->maxRows(10),
                ]),

            ]),
        ];
    }

    public function store(VideoRequest $request)
    {
        $cars = $request['cars'];
        $this->video = new Video();
        $this->video->fill($request->validated())->save();
        $this->video->cars()->sync($cars);
        if ($request->videos) {
            $this->video->attachment()->sync(
                $request->input('videos', [])
            );
            $this->video->update(['file' => $this->video->attachment()->first()->url]);
        }
        $this->video->update(['order' => Video::getMaxOrder() + 1]);
        Alert::info(__('Успешно добавили!'));
        return redirect()->route('platform.video.list');
    }
}
