<?php

namespace App\Orchid\Screens;

use App\Orchid\Layouts\Statistics\VideoStatisticsTable;
use Illuminate\Support\Facades\DB;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;

class VideoStatisticScreen extends Screen
{
    private $records;

    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(): iterable
    {
        $this->records = DB::table('car_video')->whereNotNull('time')->orderBy('time', 'asc')->paginate(10);
        return [
            'records' => $this->records
        ];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Статистика по видео';
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make()
                ->icon('bs.download')
                ->route('export-staticTable')
        ];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            VideoStatisticsTable::class
        ];
    }
}
