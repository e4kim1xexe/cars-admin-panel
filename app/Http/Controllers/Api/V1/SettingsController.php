<?php

namespace App\Http\Controllers\Api\V1;

use App\Console\Commands\CallWeatherCurrencyCommand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class SettingsController extends ApiV1Controller
{
    public function getSettings(Request $request)
    {
        $data = Cache::get('weather_data');
        if (!$data){
            $new_data = new CallWeatherCurrencyCommand();
            $new_data->handle();
            $data = Cache::get('weather_data');
        }
        json_encode($data);
        return response()->json([
            'message' => "success",
            'data' => $data
        ]);
    }
}
