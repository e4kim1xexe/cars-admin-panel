<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Driver;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class AuthController extends ApiV1Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request): JsonResponse
    {
        $login = $request->get('login');
        $driver = Driver::where('login', $login)->first();
        if (!$driver || !$login) {
            return $this->notFound();
        }
        $token = $driver->createToken($driver->login)->plainTextToken;
        return response()->json([
            'token' => $token,
            'id' => strval($driver->id),
            'login' => strval($driver->login)
        ])->header('Authorization', 'Bearer ' . $token);
    }
}
