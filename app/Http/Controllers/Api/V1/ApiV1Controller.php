<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;

class ApiV1Controller extends Controller
{
    protected function success(array|Collection|Model $data = [])
    {
        return response()->json([
            'data' => $data,
            'message' => 'success'
        ]);
    }

    protected function notFound(array|Collection|Model $data = []): JsonResponse
    {
        return response()->json([
            'data' => null,
            'message' => 'not found'
        ], 404);
    }

}
