<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Video;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class VideoController extends ApiV1Controller
{
    public function __construct()
    {
        $this->middleware('auth')->only(['storeOrUpdateWeather', 'storeOrUpdateWeatherAfter']);
    }

    public function storeOrUpdateWeather(Request $request): JsonResponse
    {
        $id = $request['id'];
        $data = $request->validate([
            'id' => 'required',
            'weather' => 'required'
        ]);
        $video = Video::find($id);
        $video->update($data);
        return $this->success($video);
    }

    public function storeOrUpdateWeatherAfter(Request $request): JsonResponse
    {
        $id = $request['id'];
        $data = $request->validate([
            'id' => 'required',
            'weather_after' => 'required'
        ]);
        $video = Video::find($id);
        $video->update($data);
        return $this->success($video);
    }

}
