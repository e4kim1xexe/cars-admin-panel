<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Car;
use App\Models\Video;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CardController extends ApiV1Controller
{
    /**
     * @param Request $request
     * @param $card_id
     * @return JsonResponse
     */
    public function makeOnline(Request $request, $card_id): JsonResponse
    {
        $status = $request->status;
        $newStatus = $status === 'online' ? 1 : 0;
        $car = Car::find($card_id);
        if ($car) {
            $car->update(['is_online' => $newStatus]);
            return response()->json(['message' => 'Статус успешно обновлен'], 200);
        }
        return response()->json(['error' => 'Карта не найдена'], 404);
    }

    /**
     * @return JsonResponse
     */
    public function getVideos(): JsonResponse
    {
        $videos = Video::orderBy('order')->get();

        $response = [];
        foreach ($videos as $video) {
            $repeats = [];
            if (!is_null($video->count_repeat))
                foreach ($video->count_repeat as $repeat) {
                    $repeats[] = [
                        'time' => $repeat['время'],
                        'count' => $repeat['количество повторений']
                    ];
                }
            else {
                $repeats[] = null;
            }

            $response[] = [
                'id' => $video->id,
                'filename' => $video->file,
                'extension' => $video->attachment->first()->extension,
                'url' => $video->attachment->first()->url,
                'repeats' => $repeats,
                "with_course_and_weather" => $video->weather,
                "after_video_weather" => $video->weather_after
            ];
        }
        return response()->json(['videos' => $response], 200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function sendStatistics(Request $request): JsonResponse
    {
        $driver = Auth::user();
        $video = $request->video;
        $time = $request->time;
        if (empty($video) || empty($time) || !$driver ) {
            return $this->notFound();
        }
        DB::table('car_video')->insert(
            [
                'video_id' => $video,
                'car_id' => $driver->car->id,
                'time' => $time,
                'repeat' => 1,
                'watch' => 2
            ]
        );
        return $this->success();
    }

}
