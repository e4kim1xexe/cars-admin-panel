<?php

namespace App\Http\Controllers\ImportExportExcel;

use App\Console\Commands\CallWeatherCurrencyCommand;
use App\Exports\CountWatchExport;
use App\Exports\WatchRecordsExport;
use App\Http\Controllers\Controller;
use App\ServicesImportData\Weather\Weather;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use stdClass;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class ExcelController extends Controller
{
    /**
     * @return BinaryFileResponse
     */
    public function exportSortTable(): BinaryFileResponse
    {
        return Excel::download(new CountWatchExport(), 'videos.xlsx');
    }

    /**
     * @return BinaryFileResponse
     */
    public function exportStaticTable(): BinaryFileResponse
    {
        return Excel::download(new WatchRecordsExport(), 'records.xlsx');
    }
}
