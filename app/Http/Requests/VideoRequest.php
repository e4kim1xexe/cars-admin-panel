<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VideoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required',
            'queue' => 'nullable',
            'is_playing' => 'nullable',
            'weather' => 'nullable',
            'weather_after' => 'nullable',
            'videos' => 'nullable',
            'repeats' => 'nullable',
            'cars' => 'nullable',
            'count_repeat' => 'nullable',
            'file' => 'nullable'
        ];
    }
}
