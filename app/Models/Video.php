<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Facades\DB;
use Orchid\Attachment\Attachable;
use Orchid\Platform\Concerns\Sortable;
use Orchid\Screen\AsSource;

class Video extends Model
{
    use HasFactory, AsSource, Attachable, Sortable;

    protected $casts = ['count_repeat' => 'array'];
    protected $fillable = ['name', 'order', 'weather', 'weather_after', 'count_repeat', 'file'];


    /**
     * @return BelongsToMany
     */
    public function cars(): BelongsToMany
    {
        return $this->belongsToMany(Car::class)->withPivot('time', 'repeat', 'watch');
    }

    public static function getAllWatchingCount($id)
    {
        return DB::table('car_video')
            ->where('video_id', $id)
            ->whereNotNull('time')
            ->count();
    }

    public static function getCarsWatching($id)
    {
        return DB::table('car_video')->where('video_id', $id)->get();
    }

    public static function getMaxOrder()
    {
        return Video::max('order');
    }

}
