<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Sanctum\HasApiTokens;
use Orchid\Screen\AsSource;

class Driver extends Model
{
    use HasFactory, HasApiTokens, AsSource;
    protected $fillable = ['name', 'login', 'number'];

    public function car()
    {
        return $this->hasOne(Car::class);
    }

}
