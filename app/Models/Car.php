<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Orchid\Screen\AsSource;

class Car extends Model
{
    use HasFactory, AsSource;

    protected $fillable = ['name', 'driver_id', 'is_online'];


    /**
     * @return BelongsToMany
     */
    public function videos(): BelongsToMany
    {
        return $this->belongsToMany(Video::class);
    }

    /**
     * @return string
     */
    public function getFullAttribute(): string
    {
        return $this->name . ' | количество просмотров:  ' . $this->id;
    }

    public function driver()
    {
        return $this->belongsTo(Driver::class);
    }
}
