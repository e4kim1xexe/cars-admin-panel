<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Brightness;
use App\Models\Car;
use App\Models\Driver;
use App\Models\Repeat;
use App\Models\User;
use App\Models\Video;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Orchid\Support\Facades\Dashboard;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        User::factory()->create([
            'name' => 'admin',
            'email' => 'admin',
            'password' => Hash::make('admin'),
            'permissions' => Dashboard::getAllowAllPermission()
        ]);
        $drivers = Driver::factory()->count(5)->create();
        foreach ($drivers as $driver){
            Car::factory()->create(['driver_id' => $driver->id, 'is_online' => true]);
        }
        $cars = Car::all();
        $videos = Video::factory()->count(5)->create();
        foreach ($videos as $video) {
            $video->cars()->attach($cars->pluck('id'), ['watch' => 2, 'repeat' => 1, 'time' => Carbon::createFromTime(
                rand(0, 23),
                rand(0, 59),
                0
            )->format('H:i')]);
        }
        Brightness::factory()->count(2)->create();
    }
}
