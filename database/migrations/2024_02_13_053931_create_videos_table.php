<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->id();
            $table->text('name');
            $table->json('count_repeat')->nullable();
            $table->boolean('weather')->nullable(false);
            $table->boolean('weather_after')->nullable(false);
            $table->integer('order')->default(1);
            $table->integer('queue')->nullable()->default(1);
            $table->string('file')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('videos');
    }
};
