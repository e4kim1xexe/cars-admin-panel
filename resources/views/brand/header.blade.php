@push('head')
    <link
        href="{{asset('upromo_logo.jpg')}}"
        id="favicon"
        rel="icon">
@endpush

<div class="h2 d-flex align-items-center">
    @auth
        <x-orchid-icon path="bs.house" class="d-inline d-xl-none"/>
    @endauth
</div>


<div class="h2 d-flex align-items-center">
    @auth
        <x-orchid-icon path="bs.house" class="d-inline d-xl-none"/>
    @endauth
    <p class="my-0 {{ auth()->check() ? 'd-none d-xl-block' : '' }}">
        <img src="{{asset('upromo_logo.jpg')}}" alt="logo" height="100px" width="100%" style="height: 79px; width: 165%">
        <small class="align-top opacity"></small>
    </p>
</div>

