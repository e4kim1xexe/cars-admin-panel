<div>
    @if(count($cars) != 0)
        @foreach($cars as $car)
            <a href="{{route('platform.cars.view', ['car' => $car->id])}}">
                {{$car->name}},
            </a>
        @endforeach
    @else
        Не установлено
    @endif
</div>
