$(document).ready(function () {

    handleWeatherCheckboxChangeWeatherNow();
    handleWeatherCheckboxChangeWeatherAfter();

    function handleWeatherCheckboxChangeWeatherNow() {
        $('.weather').change(function () {
            const checkboxId = $(this).attr('id');
            let checkedIs = $(this).prop('checked') ? 1 : 0;
            $.ajax({
                method: 'POST',
                headers: {
                    'X-CSRF-Token': document.head.querySelector('meta[name="csrf_token"]').content,
                },
                data: {
                    id: checkboxId,
                    weather: checkedIs
                },
                url: 'http://127.0.0.1:8000/api/v1/videos/' + checkboxId + '/weather',
                success: function (response) {
                    console.log(response);
                },
                error: function (xhr, status, error) {
                    console.error('Ошибка:', error);
                }
            });
        });
    }


    function handleWeatherCheckboxChangeWeatherAfter() {
        $('.weather-after').change(function () {
            const checkboxId = $(this).attr('id');
            let checkedIs = $(this).prop('checked') ? 1 : 0;
            $.ajax({
                headers: {
                    'X-CSRF-Token': document.head.querySelector('meta[name="csrf_token"]').content,
                },
                method: 'POST',
                data: {
                    id: checkboxId,
                    weather_after: checkedIs
                },
                url: 'http://127.0.0.1:8000/api/v1/videos/' + checkboxId + '/weather-after',
                success: function (response) {
                    console.log(response);
                },
                error: function (xhr, status, error) {
                    console.error('Ошибка:', error);
                }
            });
        });
    }

});
//http://127.0.0.1:8000/api/v1/videos/1/weather
