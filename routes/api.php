<?php

use App\Http\Controllers\Api\V1\AuthController;
use App\Http\Controllers\Api\V1\CardController;
use App\Http\Controllers\Api\V1\SettingsController;
use App\Http\Controllers\Api\V1\VideoController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/


Route::prefix('v1')->group(function () {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('/videos/{video}/weather', [VideoController::class, 'storeOrUpdateWeather']);
    Route::post('/videos/{video}/weather-after', [VideoController::class, 'storeOrUpdateWeatherAfter']);

    Route::middleware('auth:sanctum')->group(function () {
        Route::get('settings/', [SettingsController::class, 'getSettings']);
        Route::get('make_online/{card_id}', [CardController::class, 'makeOnline']);
        Route::get('cars/videos/', [CardController::class, 'getVideos']);
        Route::get('send_statistics/', [CardController::class, 'sendStatistics']);
    });
});


