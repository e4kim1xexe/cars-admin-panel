<?php

declare(strict_types=1);

use App\Orchid\Screens\Brightness\BrigntnessCreateScreen;
use App\Orchid\Screens\Brightness\BrigntnessEditScreen;
use App\Orchid\Screens\Brightness\BrigntnessListScreen;
use App\Orchid\Screens\Brightness\BrigntnessViewScreen;
use App\Orchid\Screens\Car\CarCreateScreen;
use App\Orchid\Screens\Car\CarEditScreen;
use App\Orchid\Screens\Car\CarListScreen;
use App\Orchid\Screens\Car\CarViewScreen;
use App\Orchid\Screens\Driver\DriversCreateScreen;
use App\Orchid\Screens\Driver\DriversEditScreen;
use App\Orchid\Screens\Driver\DriversListScreen;
use App\Orchid\Screens\Driver\DriversViewScreen;
use App\Orchid\Screens\User\UserEditScreen;
use App\Orchid\Screens\User\UserListScreen;
use App\Orchid\Screens\User\UserProfileScreen;
use App\Orchid\Screens\Video\VideoCreateScreen;
use App\Orchid\Screens\Video\VideoEditScreen;
use App\Orchid\Screens\Video\VideoListScreen;
use App\Orchid\Screens\Video\VideoViewScreen;
use App\Orchid\Screens\VideoSortListScreen;
use App\Orchid\Screens\VideoStatisticScreen;
use Illuminate\Support\Facades\Route;
use Tabuna\Breadcrumbs\Trail;

/*
|--------------------------------------------------------------------------
| Dashboard Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the need "dashboard" middleware group. Now create something great!
|
*/

// Main
Route::screen('/main', VideoListScreen::class)->name('platform.main');



// Platform > Profile
Route::screen('profile', UserProfileScreen::class)
    ->name('platform.profile')
    ->breadcrumbs(fn(Trail $trail) => $trail
        ->parent('platform.index')
        ->push(__('Profile'), route('platform.profile')));

// Platform > System > Users > User
Route::screen('users/{user}/edit', UserEditScreen::class)
    ->name('platform.systems.users.edit')
    ->breadcrumbs(fn(Trail $trail, $user) => $trail
        ->parent('platform.systems.users')
        ->push($user->name, route('platform.systems.users.edit', $user)));

// Platform > System > Users > Create
Route::screen('users/create', UserEditScreen::class)
    ->name('platform.systems.users.create')
    ->breadcrumbs(fn(Trail $trail) => $trail
        ->parent('platform.systems.users')
        ->push(__('Create'), route('platform.systems.users.create')));

// Platform > System > Users
Route::screen('users', UserListScreen::class)
    ->name('platform.systems.users')
    ->breadcrumbs(fn(Trail $trail) => $trail
        ->parent('platform.index')
        ->push(__('Users'), route('platform.systems.users')));


//CARS
Route::screen('/cars', CarListScreen::class)->name('platform.cars.list');
Route::screen('/cars/create', CarCreateScreen::class)->name('platform.cars.create');
Route::screen('/cars/{car}', CarViewScreen::class)->name('platform.cars.view');
Route::screen('/cars/{car}/edit', CarEditScreen::class)->name('platform.cars.edit');

//brightness

Route::screen('/brightnesses', BrigntnessListScreen::class)->name('platform.brightness.list');
Route::screen('/brightnesses/create', BrigntnessCreateScreen::class)->name('platform.brightness.create');
Route::screen('/brightnesses/{brightness}', BrigntnessViewScreen::class)->name('platform.brightness.view');
Route::screen('/brightnesses/{brightness}/edit', BrigntnessEditScreen::class)->name('platform.brightness.edit');

//video
Route::screen('/videos', VideoListScreen::class)->name('platform.video.list');
Route::screen('/videos/create', VideoCreateScreen::class)->name('platform.video.create');
Route::screen('/videos/{video}', VideoViewScreen::class)->name('platform.video.view');
Route::screen('/videos/{video}/edit', VideoEditScreen::class)->name('platform.video.edit');


//
Route::screen('/drivers', DriversListScreen::class)->name('platform.driver.list');
Route::screen('/drivers/create', DriversCreateScreen::class)->name('platform.driver.create');
Route::screen('/drivers/{driver}', DriversViewScreen::class)->name('platform.driver.view');
Route::screen('/drivers/{driver}/edit', DriversEditScreen::class)->name('platform.driver.edit');


//
Route::screen('/sort/videos', VideoSortListScreen::class)->name('platform.video-sort.list');
Route::screen('/statistics/videos', VideoStatisticScreen::class)->name('platform.video-static.list');
